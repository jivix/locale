<?php

declare(strict_types=1);

namespace Jivix\Locale\Resources;

final class Script
{
    /**
     * A list of ISO 15924 alpha-4 script codes.
     *
     * @see http://www.unicode.org/iso15924/
     * @var array
     */
    private static array $scripts = [
        'Adlm', 'Afak', 'Aghb', 'Ahom', 'Arab', 'Aran', 'Armi', 'Armn', 'Avst', 'Bali', 'Bamu', 'Bass', 'Batk', 'Beng',
        'Blis', 'Bopo', 'Brah', 'Brai', 'Bugi', 'Buhd', 'Cakm', 'Cans', 'Cari', 'Cham', 'Cher', 'Cirt', 'Copt', 'Cprt',
        'Cyrl', 'Cyrs', 'Deva', 'Dsrt', 'Dupl', 'Egyd', 'Egyh', 'Egyp', 'Elba', 'Ethi', 'Geok', 'Geor', 'Glag', 'Goth',
        'Gran', 'Grek', 'Gujr', 'Guru', 'Hang', 'Hani', 'Hano', 'Hans', 'Hant', 'Hatr', 'Hebr', 'Hira', 'Hluw', 'Hmng',
        'Hrkt', 'Hung', 'Inds', 'Ital', 'Java', 'Jpan', 'Jurc', 'Kali', 'Kana', 'Khar', 'Khmr', 'Khoj', 'Kitl', 'Kits',
        'Knda', 'Kore', 'Kpel', 'Kthi', 'Lana', 'Laoo', 'Latf', 'Latg', 'Latn', 'Lepc', 'Limb', 'Lina', 'Linb', 'Lisu',
        'Loma', 'Lyci', 'Lydi', 'Mahj', 'Mand', 'Mani', 'Marc', 'Maya', 'Mend', 'Merc', 'Mero', 'Mlym', 'Modi', 'Mong',
        'Moon', 'Mroo', 'Mtei', 'Mult', 'Mymr', 'Narb', 'Nbat', 'Nkgb', 'Nkoo', 'Nshu', 'Ogam', 'Olck', 'Orkh', 'Orya',
        'Osge', 'Osma', 'Palm', 'Pauc', 'Perm', 'Phag', 'Phli', 'Phlp', 'Phlv', 'Phnx', 'Plrd', 'Prti', 'Qaaa', 'Qabx',
        'Rjng', 'Roro', 'Runr', 'Samr', 'Sara', 'Sarb', 'Saur', 'Sgnw', 'Shaw', 'Shrd', 'Sidd', 'Sind', 'Sinh', 'Sora',
        'Sund', 'Sylo', 'Syrc', 'Syre', 'Syrj', 'Syrn', 'Tagb', 'Takr', 'Tale', 'Talu', 'Taml', 'Tang', 'Tavt', 'Telu',
        'Teng', 'Tfng', 'Tglg', 'Thaa', 'Thai', 'Tibt', 'Tirh', 'Ugar', 'Vaii', 'Visp', 'Wara', 'Wole', 'Xpeo', 'Xsux',
        'Yiii', 'Zinh', 'Zmth', 'Zsym', 'Zxxx', 'Zyyy', 'Zzzz'
    ];

    /**
     * Returns a list of all alpha-4 script codes defined in ISO 15924.
     *
     * @return array
     */
    public static function getISOScripts(): array
    {
        return self::$scripts;
    }
}