<?php

declare(strict_types=1);

namespace Jivix\Locale\Exceptions;

use LogicException;

class InvalidLanguageTagException extends LogicException
{
    public function __construct()
    {
        parent::__construct('Invalid IETF BCP 47 language tag.');
    }
}