<?php

declare(strict_types=1);

namespace Jivix\Locale\Exceptions;

use LogicException;

class InvalidLanguageException extends LogicException
{
    public function __construct()
    {
        parent::__construct('Invalid ISO 639 alpha-2 language code.');
    }
}