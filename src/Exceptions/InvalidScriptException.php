<?php

declare(strict_types=1);

namespace Jivix\Locale\Exceptions;

use LogicException;

class InvalidScriptException extends LogicException
{
    public function __construct()
    {
        parent::__construct('Invalid ISO 3166 alpha-2 country code.');
    }
}