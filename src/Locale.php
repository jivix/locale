<?php

declare(strict_types=1);

namespace Jivix\Locale;

use Jivix\Locale\Exceptions\InvalidCountryException;
use Jivix\Locale\Exceptions\InvalidLanguageException;
use Jivix\Locale\Exceptions\InvalidLanguageTagException;
use Jivix\Locale\Exceptions\InvalidScriptException;
use Jivix\Locale\Resources\Country;
use Jivix\Locale\Resources\Language;
use Jivix\Locale\Resources\Script;
use Serializable;

class Locale implements Serializable
{
    /**
     * Locale constants
     */
    const CHINESE_SIMPLIFIED = 'zh_Hans';
    const CHINESE_TRADITIONAL = 'zh_Hant';

    /**
     * @var string
     */
    private string $language;

    /**
     * @var string|null
     */
    private ?string $country;

    /**
     * @var string|null
     */
    private ?string $variant;

    /**
     * @var string|null
     */
    private ?string $script;

    /**
     * Construct a locale from language, country, variant and script.
     * This constructor normalizes the language value to lowercase and the country value to uppercase.
     *
     * @param string $language ISO 639-1 alpha-2 language code
     * @param string $country ISO 3166-1 alpha-2 country code
     * @param string $script ISO 15924 alpha-4 script code
     * @param string $variant Any arbitrary value used to indicate a variation of a Locale
     */
    public function __construct(string $language, ?string $country = null, ?string $script = null, ?string $variant = null)
    {
        // Language
        if (preg_match('/^[a-z]{2,3}/', $language) && in_array($language, Language::getISOLanguages())) {
            $this->language = $language;
        } else {
            throw new InvalidLanguageException();
        }

        // Script
        if (null !== $script) {
            $script = ucfirst($script);
            if (preg_match('/^[a-zA-Z]{4}/', $script) && in_array($script, Script::getISOScripts())) {
                $this->script = $script;
            } else {
                throw new InvalidScriptException();
            }
        } else {
            $this->script = null;
        }

        // Country
        if (null !== $country) {
            if (preg_match('/^[A-Z]{2}/', $country) && in_array($country, Country::getISOCountries())) {
                $this->country = $country;
            } else {
                throw new InvalidCountryException();
            }
        } else {
            $this->country = null;
        }

        // Variant
        if (null !== $variant && preg_match('/^[0-9a-zA-Z-_]*/', $variant)) {
            $this->variant = $variant;
        } else {
            $this->variant = null;
        }
    }

    /**
     * @param Locale $locale
     * @return string
     */
    public function getDisplayName(Locale $locale = null): string
    {
        return \Locale::getDisplayName($this->toLanguageTag(), ($locale ?? $this)->toLanguageTag());
    }

    /**
     * @param Locale|null $locale
     * @return string
     */
    public function getDisplayLanguage(Locale $locale = null): string
    {
        return \Locale::getDisplayLanguage($this->toLanguageTag(), ($locale ?? $this)->toLanguageTag());
    }

    /**
     * @param Locale|null $locale
     * @return string|null
     */
    public function getDisplayScript(Locale $locale = null): ?string
    {
        return null !== $this->script
            ? \Locale::getDisplayScript($this->toLanguageTag(), ($locale ?? $this)->toLanguageTag())
            : null;
    }

    /**
     * @param Locale|null $locale
     * @return string|null
     */
    public function getDisplayCountry(Locale $locale = null): ?string
    {
        return null !== $this->country
            ? \Locale::getDisplayRegion($this->toLanguageTag(), ($locale ?? $this)->toLanguageTag())
            : null;
    }

    /**
     * @param Locale|null $locale
     * @return string|null
     */
    public function getDisplayVariant(Locale $locale = null): ?string
    {
        return null !== $this->variant
            ? \Locale::getDisplayVariant($this->toLanguageTag(), ($locale ?? $this)->toLanguageTag())
            : null;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @return string|null
     */
    public function getVariant(): ?string
    {
        return $this->variant;
    }

    /**
     * @return string|null
     */
    public function getScript(): ?string
    {
        return $this->script;
    }

    /**
     * @return string|null
     */
    public function getISO3Language(): ?string
    {
        $languages = Language::getLanguages();

        return $languages[$this->language] ?? null;
    }

    /**
     * @return string|null
     */
    public function getISO3Country(): ?string
    {
        $countries = Country::getCountries();

        return $countries[$this->country] ?? null;
    }

    /**
     * Returns an array of all locales.
     *
     * @return array
     */
    public function getAvailableLocales(): array
    {
        $locales = [];
        foreach (Language::getISOLanguages() as $language) {
            $locales[] = new self($language);
        }
        return $locales;
    }

    /**
     * Returns a locale for the specified IETF BCP 47 language tag.
     *
     * @param string $languageTag IETF BCP 47 language tag
     * @return Locale The locale that best represents the language tag
     * @throws InvalidLanguageTagException
     */
    public static function forLanguageTag(string $languageTag): Locale
    {
        $language = null;
        $country = null;
        $script = null;
        $variant = null;

        $matches = [];
        if (preg_match('/^([a-z]{2,3})[_-]{1}([a-zA-Z]{2,4})[_-]?([0-9a-zA-Z-_#]*)/', $languageTag, $matches)) {
            // Language
            if (in_array($matches[1], Language::getISOLanguages())) {
                $language = $matches[1];
            } else {
                throw new InvalidLanguageTagException();
            }
            // Country or Script
            if (in_array($matches[2], Country::getISOCountries())) {
                $country = $matches[2];
            } else if (in_array($matches[2], Script::getISOScripts())) {
                $script = $matches[2];
            } else {
                throw new InvalidLanguageTagException();
            }
            // Country, Script or Variant(s)
            if (preg_match('/^([a-zA-Z]{2,8})[_-]?([0-9a-zA-Z-_#]*)/', $matches[3], $matches)) {
                $variants = [];
                if (empty($country) && in_array($matches[1], Country::getISOCountries())) {
                    $country = $matches[1];
                } else if (empty($script) && in_array($matches[1], Script::getISOScripts())) {
                    $script = $matches[1];
                } else {
                    $variants[] = $matches[1];
                }
                if (!empty($matches[2])) {
                    $parts = explode('_', str_replace('-', '_', $matches[2]));

                    foreach ($parts as $part) {
                        if (preg_match('/^[0-9a-zA-Z]{3,8}/', $part)) {
                            $variants[] = $part;
                        }
                    }
                }
                $variant = implode('_', $variants);
            }
        } else if (preg_match('/^([a-z]{2,3})$/', $languageTag, $matches)) {
            if (in_array($matches[1], Language::getISOLanguages())) {
                $language = $matches[1];
            } else {
                throw new InvalidLanguageTagException();
            }
        } else {
            throw new InvalidLanguageTagException();
        }

        return new self($language, $country, $script, $variant);
    }

    /**
     * Checks if the given string is a valid IETF BCP 47 language tag.
     *
     * @param string $languageTag IETF BCP 47 language tag
     * @return bool
     */
    public static function isValidLanguageTag(string $languageTag): bool
    {
        try {
            Locale::forLanguageTag($languageTag);

            return true;
        } catch (InvalidLanguageTagException $e) {
            return false;
        }
    }

    /**
     * Returns a well-formed IETF BCP 47 language tag representing this locale.
     *
     * Note: Although the language tag created by this method is well-formed (satisfies the syntax requirements defined by the IETF BCP 47 specification), it is not necessarily a valid BCP 47 language tag. For example,
     * new Locale('xx', 'YY')->toLanguageTag();
     * will return "xx-YY", but the language subtag "xx" and the region subtag "YY" are invalid because they are not registered in the IANA Language Subtag Registry.
     *
     * @param string $separator
     * @return string a BCP47 language tag representing the locale
     */
    public function toLanguageTag(string $separator = '-'): string
    {
        $languageTag = $this->language;

        if (null !== $this->script) {
            $languageTag .= $separator . $this->script;
        }

        if (null !== $this->country) {
            $languageTag .= $separator . $this->country;
        }

        if (null !== $this->variant) {
            $languageTag .= $separator . $this->variant;
        }

        return $languageTag;
    }

    /**
     * Returns a string representation of this object.
     *
     * @return string
     */
    public final function toString(): string
    {
        return $this->toLanguageTag();
    }

    /**
     * Returns the string representation of this object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->toString();
    }

    /**
     * Generates a storable representation of the object.
     *
     * @return string
     */
    public function serialize()
    {
        return serialize([
            'language' => $this->language,
            'script' => $this->script,
            'country' => $this->country,
            'variant' => $this->variant
        ]);
    }

    /**
     * Creates an object from a stored representation.
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $unserialized = unserialize($serialized);

        $this->language = $unserialized['language'];
        $this->script = $unserialized['script'];
        $this->country = $unserialized['country'];
        $this->variant = $unserialized['variant'];
    }
}
